﻿$(function(){
	
	$('body').prepend($('<div/>',{
    'style': 'height: 10px; background: #E4E4E4; position: fixed; z-index: 99999; width: 100%;',
	'id':'scroller',
    'html': '<span style="display:block;width:100%; height: 10px; background: #F2F2F4; float:right;"></span>'
	}));
	
	$('body').css('overflow','hidden');
		
	$('#player').css('top','10px');
	
	var w = $('#scroller span').width() , max_w = w;
		
	var body_t = 0, body_top_max = $('body').height(), window_h = $(window).height();
	
	// this w width is 100% and 100% is body_top_max
	// alert( (body_top_max/100)*1 );
	var step = (w/100)*3;
	var step_down = ((body_top_max-window_h)/100)*3;
	
	// scroll_stop
	scroll_stop = body_top_max-window_h;
	
	// using on
	$(window).on('mousewheel', function(event) {		
		
		// scrolling up
		if ( event.deltaY > 0 ) {
				
			w+=step; if ( w >= max_w ) w = max_w; $('#scroller span').css('width', w);
			
			body_t+=step_down; if ( body_t > 0 ) body_t = 0;
			
			$('body').css('margin-top',body_t+'px');
			
		// scrolling down
						
		} else {
					
			w-=step;  if (w <= 0 ) w = 0; $('#scroller span').css('width', w);
			
			body_t-=step_down; if ( Math.abs(body_t) >= scroll_stop ) body_t = -scroll_stop;
						
			$('body').css('margin-top',body_t+'px');		
		}
		
		$('#scroller').css('top',0);
		
	});

});